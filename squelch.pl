#! /usr/bin/env perl
################################################################################
# squelch.pl
#
# A script to reduce LaTeX compiler noise and parse useful warnings and errors,
# and pretty print them. It gets its name from RF nomenclature; carrier squelch
# suppresses noise until a sufficiently high amplitude signal is received.
# That's what we're doing here with the LaTeX compiler output.
#
# Args:
#  --latex-log      : The log file produced by latexmk.
#  --print-errors   : Print all errors found.
#  --print-warnings : Print all warnings found.
#  --verbose        : Print informational messages to stdout.
#
################################################################################

use strict;
use warnings;
use Getopt::Long;
use Cwd;

my %globopts;

sub cli_parse {
    # Just a single, mandatory argument for now.
    GetOptions(
        'latex-log=s'    => \$globopts{log},
        'print-errors'   => \$globopts{errors},
        'print-warnings' => \$globopts{warnings},
        'verbose'        => \$globopts{verbose},
    );

    if (!defined($globopts{log})) {
        die("--latex-log requires an argument.\n");
    }
}

sub error_print {
    my ($file, $line, $short_err, $long_err) = @_;

    print("\x1b[1;31mERROR\x1b[0m \x1b[1;37m$file:$line\x1b[0m\n" .
          "\x1b[1;37m>\x1b[0m $short_err\n" .
          "$long_err\n\n");
}

sub log_parse_errors {
    my ($slurp, $errors_ref) = @_;
    my $errors = 0;

    # Parse LaTeX errors of the format file:line:error\nlong_error
    # This assumes the pdflatex flag `-file-line-error` is being used (This
    # makes parsing errors _a lot_ easier.
    while ($slurp =~ /^(\/.*):(\d+):\s(.*)((.|\n)*?)^$/gm) {
        my ($file, $line, $error, $message) = ($1, $2, $3, $4);
        my $dir = getcwd();
        $file =~ s/$dir\///;
        my %error = (
            file    => $file,
            line    => $line,
            error   => $error,
            message => $message,
        );
        push(@{$errors_ref}, \%error);
        $errors++;
    }

    return $errors;
}

sub warning_print {
    # Number is just here for debugging. It is otherwise not informative. As
    # such, we don't print it as part of a warning message to the user.
    my ($number, $file, $line, $short_err) = @_;

    print("\x1b[1;33mWARNING:\x1b[0m \x1b[1;37m$file:$line\x1b[0m\n" .
          "\x1b[1;37m>\x1b[0m $short_err\n\n");
}

sub log_parse_warnings {
    my ($slurp, $warnings_ref) = @_;
    my $warnings = 0;

    # Now the fun begins. Since the LaTeX warning specs are all over the place,
    # it'll be necessary to have selective warning discovery rules. Prepare
    # yourself; here be dragons...
    my $dir = getcwd();
    # Get LaTeX compiler file context with all file-specific content. This one
    # needs a bit of explaining. The format is as follows:
    # [n] <stuff> (/last/path/on/line/to/file.tex\n <everything up to next [n]>
    while ($slurp =~
        /^\[(\d+)(?: <.*?>)?\].*\($dir\/([\w\/-]+\.tex)$((.|\n)*?)(?=\[\d+(?: <.*?>)?\]\s\()/gm) {
        my ($number, $file, $mess) = ($1, $2, $3);

        if (defined($globopts{verbose})) {
            print("[$number] Discovered file context: $file\n");
        }

        # Capture undefined reference warnings. Note that there are typically
        # several instances referring to the same reference in the file, but
        # we're going back to the source of the warning.
        while ($mess =~ /(LaTeX Warning:\s)(.*) on input line (\d+).$/gm) {
            my ($error, $line) = ($2, $3);

            if (defined($globopts{verbose})) {
                print("\t> Discovered undefined reference at line $line\n");
            }

            my %warning = (
                number => $number,
                file   => $file,
                line   => $line,
                error  => $error,
            );
            push(@{$warnings_ref}, \%warning);
            $warnings++;
        }
    }

    return $warnings;
}

my @errors;
my @warnings;

cli_parse();

# Open the latex compiler output log and slurp it.
open(my $fh, '<', $globopts{log})
    or die("Couldn't open $globopts{log}: $!");
$/ = undef;
my $slurp = <$fh>;
close($fh);

# Parse errors and warnings we're interested in.
my $n_errors = log_parse_errors($slurp, \@errors);
my $n_warnings = log_parse_warnings($slurp, \@warnings);

if (defined($globopts{errors}) && $n_errors != 0) {
    foreach (@errors) {
        error_print($_->{file}, $_->{line}, $_->{error}, $_->{message});
    }
}

if (defined($globopts{warnings}) && $n_warnings != 0) {
    foreach (@warnings) {
        warning_print($_->{number}, $_->{file}, $_->{line}, $_->{error});
    }
}

# Propagate error notification for make.
exit($n_errors);

